import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: () => import(/* webpackChunkName: "about" */ '../views/Dashboard.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/Dashboard',
    name: 'Dashboard',

    component: () => import(/* webpackChunkName: "about" */ '../views/Dashboard.vue')
  },
  {
    path: '/Revenue',
    name: 'Revenue',

    component: () => import(/* webpackChunkName: "about" */ '../views/Revenue.vue')
  },
  {
    path: '/Expenses',
    name: 'Expenses',

    component: () => import(/* webpackChunkName: "about" */ '../views/Expenses.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
